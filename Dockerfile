ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/jre11:latest

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install openjdk-11-jdk
